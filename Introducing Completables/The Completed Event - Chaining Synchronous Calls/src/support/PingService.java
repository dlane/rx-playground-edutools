package support;

@FunctionalInterface
public interface PingService {
  /**
   * Creates an audible "ping" on the user's computer.
   */
  void pingUser();
}
