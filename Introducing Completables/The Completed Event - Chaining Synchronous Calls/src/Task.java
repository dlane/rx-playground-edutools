import rx.Completable;
import support.PingService;

class Task {
  private final Completable coffeeBrewingCompletable;
  private final PingService pingService;

  Task(Completable coffeeBrewingCompletable, PingService pingService) {
    this.coffeeBrewingCompletable = coffeeBrewingCompletable;
    this.pingService = pingService;
  }

  /**
   * Returns a Completable which starts the coffee machine and then creates
   * an audible "ping" on the user's computer when the coffee is done.
   *
   * @return completable as described
   */
  Completable brewCoffeeAndPing() {
    return coffeeBrewingCompletable
        .doOnCompleted(pingService::pingUser);
  }

  public static void main(String[] argv) {

    // Mock the incoming services by printing to the console
    Completable coffeeCompletable = Completable.fromAction(() ->
        System.out.println("Starting brew!"));
    PingService pingService = () -> System.out.println("Ping!");


    // Call the implementation function
    Task task = new Task(coffeeCompletable, pingService);
    Completable coffeeAndPingCompletable = task.brewCoffeeAndPing();

    // A completable that calls barry when it's done
    Completable capcPingBarry = coffeeAndPingCompletable
        .doOnCompleted(() -> System.out.println("Hi Barry!"));

    Completable capcPingBarryAndFred = capcPingBarry
        .doOnCompleted(() -> System.out.println("Hi Fred!"));

    Completable capcPingBarryAndHelen = capcPingBarry
        .doOnCompleted(() -> System.out.println("Hi Helen!"));

    System.out.println();
    System.out.println("Brewing for Barry...");
    capcPingBarry.await();

    System.out.println();
    System.out.println("Brewing for Barry and Fred...");
    capcPingBarryAndFred.await();

    System.out.println();
    System.out.println("Pinging Barry and Helen");
    capcPingBarryAndHelen.await();
  }
}