import com.google.common.collect.Lists;
import rx.Completable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static junit.framework.TestCase.assertEquals;

public class Test {
    @org.junit.Test
  public void testTask() {
        ArrayList<String> arrayList = new ArrayList<>();
      Task task = new Task(Completable.fromAction(() -> {
          synchronized (arrayList) { arrayList.add("brew"); }
      }),
          () -> {synchronized(arrayList) {arrayList.add("ping");} });

      // test that after calling the function but before subscribing nothing happens
      Completable completable = task.brewCoffeeAndPing();
      synchronized (arrayList) {
            assertEquals(Lists.newArrayList(), arrayList);
        }

      // test that after a single subscribe the ping happens once
      completable.await();
      synchronized (arrayList) {
          assertEquals(Lists.newArrayList("brew", "ping"), arrayList);
      }

      // test that after two susbscibes there are two pings
      completable.await();
      synchronized (arrayList) {
          assertEquals(Lists.newArrayList("brew", "ping", "brew", "ping"),
              arrayList);
      }
    }
}