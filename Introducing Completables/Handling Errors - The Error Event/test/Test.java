import rx.Completable;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertSame;

public class Test {
  @org.junit.Test
  public void doTest() {
    AtomicReference<Throwable> atomicReference = new AtomicReference<>();
    RuntimeException runtimeException = new RuntimeException();
    Task task = new Task(Completable.error(runtimeException), atomicReference::set);
    task.brewAndHandleErrors().await();
    assertSame(atomicReference.get(), runtimeException);
  }
}