package support;

@FunctionalInterface
public interface EmailService {
  void emailMeTheException(Throwable throwable);
}
