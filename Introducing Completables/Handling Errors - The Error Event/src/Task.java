import rx.Completable;
import support.EmailService;

import java.io.IOException;

class Task {
  private final Completable brewThenPingCompletable;
  private final EmailService emailService;

  Task(Completable brewThenPingCompletable, EmailService emailService) {
    this.brewThenPingCompletable = brewThenPingCompletable;
    this.emailService = emailService;
  }

  /**
   * Brews the coffee and pings the user.
   *
   * If there is an error it will
   * - email the developers
   * - print an apology to the user
   * - complete succesfully
   */
  Completable brewAndHandleErrors() {
    return brewThenPingCompletable.doOnError(emailService::emailMeTheException)
        .doOnError(throwable ->
            System.out.println("Sorry! There's been an Error, the developers have been notified!"))
        .onErrorComplete();
  }

  public static void main(String[] argv) {

    // Create a completable that immediately sends an error event when you subscribe to it
    Completable errorCompletable = Completable.error(new IOException("EOUTOFWATER", new RuntimeException()));

    // Show that await() throws
    // Note that the doOnCompleted after the error doesn't happen
    System.out.println("Trying to await() throwing completable");
    try {
      errorCompletable
          .doOnCompleted(() -> System.out.println("Complete!"))
          .await();
    } catch (RuntimeException e) {
      // Note that it's a RuntimeException and not an IOException
      System.out.println(String.format("Got exception %s", e));
    }

    // now handle errors
    // Note that after onErrorComplete() the doOnComplete() handler is called
    // but not doOnError()
    Task task = new Task(errorCompletable,
        e -> System.out.println(String.format("Emailing developer with error: %s", e)));

    System.out.println();
    System.out.println("Now handling errors");
    task.brewAndHandleErrors()
        .doOnCompleted(() -> System.out.println("Completed!"))
        .doOnError(t -> System.out.println("Got exception again!"))
        .await();

  }
}