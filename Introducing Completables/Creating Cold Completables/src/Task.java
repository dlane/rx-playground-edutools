import com.oceannaeco.reactingtorx.util.RxUtil;
import rx.Completable;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

class Task {

  public static String EMAIL_MESSAGE = "email-to: bob@bobcom.com, message: 'put a pot on Bob!'";
  private static void makeBobMakeCoffee(Consumer<String> messageConsumer) {
    messageConsumer.accept(EMAIL_MESSAGE);
  }

  public static Completable reactiveMakeBobMakeCoffee(Consumer<String> messageConsumer) {
    return Completable.fromAction(() -> makeBobMakeCoffee(messageConsumer));
  }

  public static void main(String[] argv) throws InterruptedException {

    Completable coffeeCompletable = reactiveMakeBobMakeCoffee(System.out::println)
        // This is a bit of magic that will log standard events.
        .compose(completable -> RxUtil.wrapWithStandardEventLogging(completable, "CoffeeCompletable"));

    coffeeCompletable.await();

    // Sleep until we're really desperate, if he responds we'll stop the program
    System.out.println("Really desperate now!");
    Thread.sleep(1000);

    coffeeCompletable.await();

  }
}