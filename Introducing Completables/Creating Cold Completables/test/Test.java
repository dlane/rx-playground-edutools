import com.google.common.collect.Lists;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import rx.Completable;

import javax.swing.*;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class Test {
    @org.junit.Test
  public void doTest() {
      List<String> arrayList = new ArrayList<>();
      Completable completable = Task.reactiveMakeBobMakeCoffee(msg -> {
        synchronized (arrayList) {
          arrayList.add(msg);
        }
      });
      completable.await();
      synchronized (arrayList) {
        assertEquals(arrayList, Lists.newArrayList(Task.EMAIL_MESSAGE));
      }
      completable.await();
      synchronized (arrayList) {
        assertEquals(arrayList, Lists.newArrayList(Task.EMAIL_MESSAGE, Task.EMAIL_MESSAGE));
      }
    }
}