import rx.Completable;

class Task {
  private final Completable brewCompletable;
  private final Completable pingCompletable;

  Task(Completable brewCompletable, Completable pingCompletable) {
    this.brewCompletable = brewCompletable;
    this.pingCompletable = pingCompletable;
  }

  /**
   * Returns a completable which brews coffee then pings the user
   *
   * @return described completable
   */
  Completable brewCoffeeThenPing() {
    return brewCompletable.andThen(pingCompletable);
  }

  public static void main(String[] argv) {
    Completable brewCompletable = Completable.fromAction(() ->
        System.out.println("brew!"));
    Completable pingCompletable = Completable.fromAction(() ->
        System.out.println("ping!")
        );

    Task task = new Task(brewCompletable, pingCompletable);

    // Call our brew then ping function so we can look at the log output
    System.out.println("Brewing then pinging...");
    Completable brewPingCompletable = task.brewCoffeeThenPing();

    brewPingCompletable.await();

    // Demonstrate that subscribing for a second time causes the same routines
    // to be called.
    System.out.println();
    System.out.println("Brewing again!");
    brewPingCompletable.await();

    // Demonstrating one more time: if you can construct different, independent
    // chains from the same completable.

    System.out.println();
    System.out.println("Brewing for Barry...");
    brewPingCompletable.andThen(Completable.fromAction(() ->
        System.out.println("Brewing complete for Barry!")))
        .await();

    System.out.println();
    System.out.println("Brewing for Helen...");
    brewPingCompletable.andThen(Completable.fromAction(() ->
        System.out.println("Brewing Complete for Helen!")))
      .await();

    // One last time, now demonstrating the static version
    System.out.println();
    System.out.println("Brewing for Fred");
    Completable.concat(brewPingCompletable, Completable.fromAction(() ->
        System.out.println("Brewing complete for Fred!")))
        .await();


  }
}