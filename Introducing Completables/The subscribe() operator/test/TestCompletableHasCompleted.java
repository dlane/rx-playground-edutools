import org.junit.Test;
import rx.Completable;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.Assert;
import rx.schedulers.Schedulers;

public class TestCompletableHasCompleted {
  private static void sleepForThreeSeconds() {
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  @Test
  public void testBlockingCompletable() {
    AtomicBoolean atomicBoolean = new AtomicBoolean(false);
    Completable completable = Completable.fromAction(TestCompletableHasCompleted::sleepForThreeSeconds)
      .doOnCompleted(() -> atomicBoolean.set(true));
    Task.subscribeToBlockingCompletable(completable);
    Assert.assertTrue("Check completable has completed", atomicBoolean.get());
  }

  @Test
  public void testNonBlockingCompletable() throws InterruptedException {
    AtomicBoolean atomicBoolean = new AtomicBoolean(false);
    Completable completable = Completable.fromAction(TestCompletableHasCompleted::sleepForThreeSeconds)
        .subscribeOn(Schedulers.newThread())
        .doOnCompleted(() -> atomicBoolean.set(true));
    Task.waitOnNonBlockingCompletable(completable);
    Assert.assertTrue("Check completable has completed", atomicBoolean.get());
  }
    // put your test here
}