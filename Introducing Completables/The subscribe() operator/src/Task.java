import rx.Completable;

class Task {
  static void subscribeToBlockingCompletable(Completable blockingCompletesAfterThreeSeconds) {
    blockingCompletesAfterThreeSeconds.subscribe();
  }

  static void waitOnNonBlockingCompletable(Completable completesAfterThreeSeconds) throws InterruptedException {
    completesAfterThreeSeconds.subscribe();
    Thread.sleep(4000);
  }
}