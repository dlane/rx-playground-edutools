import org.junit.Test;
import rx.Completable;
import rx.schedulers.Schedulers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class TestCompletable {

    private static final String SUCCESS = "Completable has completed!";
    private static final String FAILURE = "Completable has not yet completed!";
    @Test
    public void testWaitForCompletable() {
      AtomicReference<String> atomicReference = new AtomicReference<>(FAILURE);
      Completable completable = Completable.fromAction(() -> {
          try {
            Thread.sleep(3000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
      }).subscribeOn(Schedulers.newThread())
              .doOnCompleted(() -> atomicReference.set(SUCCESS));
      Task.waitForCompletable(completable);

      assertThat(atomicReference.get(), equalTo(SUCCESS));

    }
}