import com.oceannaeco.reactingtorx.util.CompletableFactory;
import rx.Completable;

import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.System.out;

class Task {
  public static void waitForCompletable(Completable completable) {
    completable.await();
  }

  public static void main(String[] argv) {
    Completable completable = CompletableFactory.simpleLoggingCompletable("My First Completable");
    waitForCompletable(completable);
  }
}