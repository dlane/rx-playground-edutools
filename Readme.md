# Reacting to Rx Java 1

While learning Rx Java I spent my time wondering around
courses that were very practical and got you started immediately.

This was great, until later on I found that because I lacked a theoretical understanding of what was meant
to be going on I was poking in the dark at what is quite a different way of writing code. All my functions ended up
feeling dirty and maintenance was increasingly difficult.

This guide is meant to fill that niche. It's practical, so you can play with and immediately get a feel for the code you are writing.
It also goes in the other direction to the other guide's I've read: most of the time you'll start with an Observable,
jump straight in to schedulers and flat-maps. There's so much going on at this point that it's
difficult to see what's going on anywhere! In this guide we start with the simplest case (subscribing to a Completable),
take a real look at what's actually going on and then slowly move up from there.

I hope that by the end of this guide you will not only be able to program reactively but also have a good grounding on
how to build complex and maintainable programs.

## Notes

This is a work in progress in very early stages- feedback is welcome!

I've not had the opportunity to work with Hot Observables, so the coverage in this particular guide may be minimal 
until further notice.

## Using the Framework

This guide uses the IntelliJ EduTools plugin. If you have pulled it from a git repository you will need to
install the plugin, select the "Educator" role and then re-open the project. After this run
`File-> Course Creator -> Run Course Preview`.

There is one significant flaw in the EduTools framework: you get limited feedback from tests. It's usually desirable
to be able to print out the event ordering from a reactive program - in order to do this tasks include an additional
`Main` function that can be run from the IDE and will print illustrative console output from the tests.