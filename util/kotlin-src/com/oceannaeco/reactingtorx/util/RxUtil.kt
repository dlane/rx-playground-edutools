@file:JvmName("RxUtil")
package com.oceannaeco.reactingtorx.util

import rx.Completable

fun logWithThread(format: String, vararg args : Unit) =
    System.out.println(String.format("${Thread.currentThread().name}: $format", *args))

fun wrapWithStandardEventLogging(completable: Completable, name: String) : Completable {
    val threadName = Thread.currentThread().name
    return completable.doOnSubscribe{ println("$threadName: '$name' received subscribe event") }
            .doOnCompleted{println("$threadName: '$name' received completed event")}
            .doOnError{println("$threadName: '$name' received error event: $it")}
            .doOnTerminate { println("$threadName: '$name' has terminated") }
}
