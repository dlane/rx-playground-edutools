@file:JvmName("CompletableFactory")
package com.oceannaeco.reactingtorx.util

import rx.Completable
import rx.Scheduler
import rx.schedulers.Schedulers

/**
 * Creates a completable that executes on a new thread scheduler and logs events:
 * - subscribed
 * - completed
 * - error
 * - terminated
 *
 * along with the thread they are running on
 *
 * @param   name Name to print when logging
 */
fun simpleLoggingCompletable(name: String) =
        Completable.fromAction{ logWithThread("Subscribed to Completable '$name'")}
        .subscribeOn(Schedulers.newThread())
                .doOnCompleted{ logWithThread("Completable '$name' has Completed successfully")}
                .doOnError{ logWithThread("Completable '$name' has thrown an error: $it")}
                .doOnTerminate{ logWithThread("Completable '$name' has terminated")}
